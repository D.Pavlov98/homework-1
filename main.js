/**
 * Задание 1
 * 
 * Напишите функцию, которая принимает на вход массив строк 
 * и возвращает только те их них что короче 20 символов
 * 
 * [
 *   'Я короткая строка',
 *   'Я вроде бы тоже короткая',
 *   'А я длинная строка' 
 * ] => [
 *   'Я короткая строка',
 *   'А я длинная строка' 
 * ]
 * 
 * Используем: filter
 */

// const arrStr=['Я короткая строка',
//             'Я вроде бы тоже короткая',
//             'А я длинная строка'];
//             function getTwentySymbol(arrStr){
//                 const result=arrStr.filter(function(a){
//                     return a.length<20;
//                 })
//                 return result;
//             }
//             const result=getTwentySymbol(arrStr);
//             console.log(result);






/**
 * Задание 2
 * 
 * Напишите функцию, которая принимает на вход следующие данные:
 * 
 * [
 *   { name: 'shark', likes: 'ocean' },
 *   { name: 'turtle', likes: 'pond' },
 *   { name: 'otter', likes: 'fish biscuits' }
 * ]
 * 
 * И возвращает массив строк:
 * 
 * [ 'shark likes ocean', 'turtle likes pond', 'otter likes fish biscuits' ]
 * 
 * Используем: map
 */



// const arr=[
//         { name: 'shark', likes: 'ocean' },
//         { name: 'turtle', likes: 'pond' },
//         { name: 'otter', likes: 'fish biscuits' }
//             ]

// function getTotal(arr){
//     return arr.map(arr=>arr.name + ' likes ' + arr.likes);
// }

// console.log(getTotal(arr));


/**
 * Задание 3
 * 
 * Напишите функцию, которая принимает на вход 2 объекта 
 * и возвращает 1 со общими свойствами
 * 
 * { name: 'Алиса' }, { age: 11 } => { name: 'Алиса', age: 11 } 
 * 
 * Используем: ...
 */


// const obj1 = {name:'Алиса'};
// const obj2 = { age:11};
// function getInfoGirl(obj1,obj2){
//     return {...obj1,...obj2};
// }
// const obj3=getInfoGirl(obj1,obj2);
// console.log(obj3);




/**
 * Задание 4
 * 
 * Напишите функцию, которая возвращает наименьшее значение массива
 * [1,2,3,4] => 1
 * 
 * Используем: оператор ... и Math.min
 */


// const arr=[1,2,3,4];
// function getMin(arr){
// return Math.min(...arr);
// }
// console.log(getMin(arr));



/**
 * Задание 5
 * 
 * Напишите функцию, которая возвращает нечетные значения массива.
 * [1,2,3,4] => [1,3]
 * 
 * Используем: reduce
 */


// const arr=[1,2,3,4];
// function getValue(arr){
//     return arr.reduce(function(acc,currentValue){
//         return currentValue%2===1 ?[...acc,currentValue] :acc
//     },[])
// }
// console.log(getValue(arr));




/**
 * Задание 2
 * 
 * Напишите функцию, которая принимает на вход данные из корзины в следующем виде:
 * 
 * [
 *   { price: 10, count: 2},
 *   { price: 100, count: 1},
 *   { price: 2, count: 5},
 *   { price: 15, count: 6},
 * ]
 * где price это цена товара, а count количество.
 * 
 * Функция должна вернуть итоговую сумму по данному заказу.
 * 
 * Используем: reduce
 */


// const arr=[ { price: 10, count: 2},
//             { price: 100, count: 1},
//             { price: 2, count: 5},
//             { price: 15, count: 6}, ];
            
//             const total=arr.reduce(function(acc,value){
//             return acc+ value.price*value.count;
//             },0)
// console.log(total);



/**
 * Задание 3
 * 
 * Реализовать функцию, на входе которой массив чисел, на выходе массив уникальных значений
 * [1, 2, 2, 4, 5, 5] => [1, 2, 4, 5]
 * 
 * Используем: reduce и indexOf
 */



// const arr=[1,2,2,4,5,5];
// function getValues(arr){
//     return arr.reduce(function(acc,current){
//         if(acc.indexOf(current)===-1){
//             acc.push(current);
//         }
//         return acc;
//     },[])
// }
// const index=getValues(arr);
// console.log(index);

/**
 * Задание 4
 * 
 * Реализовать функцию, на входе которой число с ошибкой, на выходе строка с сообщением
 * 500 => Ошибка сервера
 * 401 => Ошибка авторизации
 * 402 => Ошибка сервера
 * 403 => Доступ запрещен
 * 404 => Не найдено
 * 
 * Используем: switch case
 */

// const error=403;
// switch(error){
//     case 500:console.log('Ошибка сервера');
//     break;
//     case 401:console.log('Ошибка авторизации');
//     break;
//     case 402:console.log('Ошибка сервера');
//     break;
//     case 403:console.log('Доступ запрещен');
//     break;
//     case 404:console.log('Не найдено');
//     break;
// }




/**
 * Задание 5
 * 
 * Напишите функцию, которая возвращает 2 наименьших значение массива
 * [4,3,2,1] => [1,2]
 * 
 * Используем: .sort() 
 */


// const arr=[4,3,2,1];
// arr.sort(function(a,b){
//     return a-b;
// })
// arr.splice(2,3);

// console.log(arr);

/**
 * Задание 6
 * 
 * Реализовать функцию, на входе которой объект следующего вида:
 * {
 *    firstName: 'Петр',
 *    secondName: 'Васильев',
 *    patronymic: 'Иванович'
 * }
 * на выходе строка с сообщением 'ФИО: Петр Иванович Васильев' 
 */


// const obj={
//         firstName: 'Петр',
//         secondName: 'Васильев',
//         patronymic: 'Иванович'
//     }
//     function getFullName(ojb){

//         return 'Фио: '+ obj.firstName +' '+ obj.secondName+' '+ obj.patronymic;
//     }
//     const result=getFullName(obj)
// console.log(result);


/**
 * Задание 7
 * 
 * Реализовать функцию, которая принимает на вход 2 аргумента: массив чисел и множитель,
 * а возвращает массив исходный массив, каждый элемент которого был умножен на множитель:
 * 
 * [1,2,3,4], 5 => [5,10,15,20]
 * 
 * Используем: map
 */


// const arr=[1,2,3,4]
// const multiplier=5;
// const result=arr.map(function(a){
//     return a*multiplier;
// })
// console.log(result);



/**
 * Задание 8
 * 
 * Реализовать функцию, которая принимает на вход 2 аргумента: массив и франшизу,
 * а возвращает строку с именнами героев разделенных запятой:
 * 
 * [
 *    {name: “Batman”, franchise: “DC”},
 *    {name: “Ironman”, franchise: “Marvel”},
 *    {name: “Thor”, franchise: “Marvel”},
 *    {name: “Superman”, franchise: “DC”}
 * ],
 * Marvel
 * => Ironman, Thor
 * 
 * Используем: filter, map, join
 */


// const arr = [ 
// {name: "Batman", franchise: "DC"}, 
// {name: "Ironman", franchise: "Marvel"}, 
// {name: "Thor", franchise: "Marvel"}, 
// {name: "Superman", franchise: "DC"} ]; 

// const universe='Marvel';
// function franchise(arr,universe){
//         const filterUniverse=arr.filter(universeName =>
//                 universeName.franchise===universe);
        
//         const heroes=filterUniverse.map(heroesName=>
//                 heroesName.name
//         );
//         const result=heroes.join(',');
//         return result;
// }
// console.log(franchise(arr,universe));



